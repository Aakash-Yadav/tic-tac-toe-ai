#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

int checkWhichMarkWon(int mark, int *board) {
    if (*(board + 0) == *(board + 1) && *(board + 0) == *(board + 2) && *(board + 0) == mark) {
        return true;
    } else if (*(board + 3) == *(board + 4) && *(board + 3) == *(board + 5) && *(board + 3) == mark) {
        return true;
    } else if (*(board + 6) == *(board + 7) && *(board + 6) == *(board + 8) && *(board + 6) == mark) {
        return true;
    } else if (*(board + 0) == *(board + 3) && *(board + 0) == *(board + 6) && *(board + 0) == mark) {
        return true;
    } else if (*(board + 1) == *(board + 4) && *(board + 1) == *(board + 7) && *(board + 1) == mark) {
        return true;
    } else if (*(board + 2) == *(board + 5) && *(board + 2) == *(board + 8) && *(board + 2) == mark) {
        return true;
    } else if (*(board + 0) == *(board + 4) && *(board + 0) == *(board + 8) && *(board + 0) == mark) {
        return true;
    } else if (*(board + 6) == *(board + 4) && *(board + 6) == *(board + 2) && *(board + 6) == mark) {
        return true;
    } else {
        return false;
    }
}
