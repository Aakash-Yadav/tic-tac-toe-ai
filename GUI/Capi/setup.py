from setuptools import setup
from Cython.Build import cythonize

setup(
    name='Hello world app',
    ext_modules=cythonize("app.pyx"),
    zip_safe=False,
)

from os import listdir,system

for i in listdir("."):
    if i == "build" or i=="app.c":
        system(f"rm -rf {i}")
    