#include "win_loss_logic.c"

int Draw(int *board){
    for(int i = 0; i < 9; i++){
        if (*(board + i) == 0){
            return 0;
        }
    }
    return 1;
}

int min_max(int *game_board,int is_min_max){

    if(checkWhichMarkWon(1, game_board)){
        return 1;
    }else if(checkWhichMarkWon(-1, game_board)){
        return -1;
    }else if(Draw(game_board)){
        return 0;
    }

    if(is_min_max == 1){
        int best_score = -100;
        int score;
        for(int i = 0; i < 9; i++){
            if(*(game_board + i) == 0){
                *(game_board + i) = 1;
                score = min_max(game_board, 0);
                *(game_board + i) = 0;
                if(score > best_score){
                    best_score = score;
                }
            }
        }
        return best_score;
    }else{
        int best_score = 1000;
        int score;
        for(int i = 0; i < 9; i++){
            if(*(game_board + i) == 0){
                *(game_board + i) = -1;
                score = min_max(game_board, 1);
                *(game_board + i) = 0;
                if(score < best_score){
                    best_score = score;
                }
            }
        }
        return best_score;
    }
}

int Computer(int *game_board){
    int copy_of_game_board[9];
    int *Copy_of_game_board = copy_of_game_board;

    for(int i = 0; i < 9; i++){
        *(Copy_of_game_board + i) = *(game_board + i);
    }

    int best_move = 0;
    int best_score = -1000;
    int score;
    for(int i = 0; i < 9; i++){
        if(*(Copy_of_game_board + i) == 0){
            *(Copy_of_game_board + i) = 1;
            
            score = min_max(Copy_of_game_board, 0);
            *(Copy_of_game_board + i) = 0;
            if(score > best_score){
                best_move = i;
                best_score = score;
            }
        }
    }
    return best_move;
}

