
from tkinter import * 
from array import array
from tkinter.messagebox import showinfo

class Game():
    
    def __init__(self,master):
        self.master = master
        self.click = True;
        self.count = 0;
        self.game_value = array("i",[0*x for x in range(9)]);
        self.__set_up_basic();
        self.__button_of_games()
        
    def __set_up_basic(self):
        WIDTH = 365;
        HIGHT = 300;
        self.master.title("Tic Tac Toe");
        self.master.geometry(f"{WIDTH}x{HIGHT}");
        self.master.config(bg="black");
    
    def __button_of_games(self):
        
        button = [ [x*0 for x in range(3)] for x in range(3) ]

        for i in range(3):
            for j in range(3):
                button[i][j] = Button(self.master,text="",width=12,height=5,command= lambda row=i,col=j,btn=button:self.display_text(row,col,btn),bg="white");
                button[i][j].grid(row=i,column=j);
    
    def a_fun_after_game(self,board):
        for i in range(3):
            for j in range(3):
                board[i][j]['text'] = "";
                board[i][j].config(bg="white")
        self.count = 0;
        self.click=True;
        # self.game_value = array("i",[0*x for x in range(9)]);
        
    
    def display_text(self,row,col,btn):
        value = {"x":1,"0":-1,"":0}
        if not btn[row][col]['text'] :
            self.count += 1;
            if self.click:
                btn[row][col].config(bg='red')
                btn[row][col].config(fg='black')
                btn[row][col]['text'] = "X";
                self.click = False;
                # self.game_value[row*col]=1
            
            else:
                btn[row][col].config(bg='green')
                btn[row][col].config(fg='black')
                btn[row][col]['text'] = "0";
                self.click = True;
                # self.game_value[row*col] = -1
            k=(sum(btn,[]))
            self.game_value = array("i",list(map(lambda n: value[n['text'].lower()],k)));
            print(self.game_value)
            
            if self.count==9:
                showinfo("Tie","No win")
                self.a_fun_after_game(btn)
                return 
            
            if self.win_loss(self.game_value)==1:
                # quit(0)
                if(not self.click):
                    print("X win")
                    showinfo("Win","X Win")
                else:
                    print("0 win")
                    showinfo("WIN","0 Win")
                self.a_fun_after_game(btn)
                return 
            # print(self.game_value)         
    
    def win_loss(self,board):
        rows = [board[i:i+3] for i in range(0, 9, 3)]
        cols = [[board[i], board[i+3], board[i+6]] for i in range(3)]
        diags = [[board[0], board[4], board[8]], [board[2], board[4], board[6]]]

        for line in rows + cols + diags:
            if sum(line) == 3:
                return 1
            elif sum(line) == -3:
                return 1
        return 0
        
                

if __name__ == "__main__":
    app = Tk();
    game = Game(app)
    mainloop()