#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void print_table(int *n)
{
    for (int i = 0; i < 9; i++)
    {
        if ((i + 1) % 3 == 0)
        {
            printf("%d  \n", *n);
        }
        else
        {
            printf("%d  ", *n);
        }
        n = n + 1;
    }
}

int winner(int w[])
{
    if ((w[0] + w[4] + w[8]) == 3 || (w[2] + w[4] + w[6]) == 3)
    {
        return 1;
    }
    else if ((w[0] + w[4] + w[8]) == -3 || (w[2] + w[4] + w[6]) == -3)
    {
        return 2;
    }

    if ((w[0] + w[1] + w[2]) == 3 || (w[3] + w[4] + w[5]) == 3 || (w[6] + w[7] + w[8]) == 3)
    {
        return 1;
    }
    else if (((w[0] + w[1] + w[2]) == -3 || (w[3] + w[4] + w[5]) == -3 || (w[6] + w[7] + w[8]) == -3))
    {
        return 2;
    }

    else if ((w[0] + w[3] + w[6] == 3) || (w[1] + w[4] + w[7] == 3) || (w[2] + w[5] + w[8] == 3))
    {
        return 1;
    }
    else if (((w[0] + w[3] + w[6] == -3) || (w[1] + w[4] + w[7] == -3) || (w[2] + w[5] + w[8] == -3)))
    {
        return 2;
    }
    else
    {
        return 0;
    }
}

int return_random(int lower, int upper, int count)
{
    int i;
    int num;
    for (i = 0; i < count; i++)
    {
        num = (rand() %
               (upper - lower + 1)) +
              lower;
    }
    return num;
}

int add_value_function(int game_bord[])
{
    int add = 0;
    while (1)
    {
        add = return_random(0, 9, 1);
        if (game_bord[add] == 0)
        {
            break;
        }
    }
    return add;
}

int ai_play(int game_bord_main[], int play_main, int turn__main)
{

    int ai = 0;
    int ai_out_key[5];
    int add_one_key[9];
    for (int i = 0; i < 10; i++)
    {
        ai_out_key[i] = 0;
    }
    int game_bord[9];
    int play, turn_;
    while (ai != 5)
    {
        play = play_main;
        turn_ = turn__main;

        for (int i = 0; i < 9; i++)
        {
            game_bord[i] = game_bord_main[i];
        }
        int add_ai_value = 0;
        do
        {
            int add_value = add_value_function(game_bord);
            add_one_key[add_ai_value];
            if (game_bord[add_value] == 0 && turn_)
            {
                game_bord[add_value] = 1;
                turn_ = 0;
            }
            else if (game_bord[add_value] == 0 && !turn_)
            {
                game_bord[add_value] = -1;
                turn_ = 1;
            }
            int winner_player = winner(game_bord);
            if (winner_player == 2)
            {
                ai_out_key[ai] = add_one_key[0];
                add_ai_value = 0;
                break;
            }
            play -= 1;
            add_ai_value++;
        } while (play >= 0);
        ai++;
        printf("Done --");
    }

    for (int i = 0; i < 5; i++)
    {
        printf("%d \n", ai_out_key[i]);
    }

    return 0;
}

void game(int value[9], int *val, int pre)
{
    int play = 9;
    int turn_ = 1;
    int add_value;
    play = play - pre;
    do
    {
        printf("\n");
        print_table(val);
        printf("Enter The position of player %d? ", turn_);
        scanf("%d", &add_value);
        add_value = add_value - 1;
        if (value[add_value] == 0 && turn_)
        {
            value[add_value] = 1;
            turn_ = 0;
        }
        else if (value[add_value] == 0 && !turn_)
        {
            value[add_value] = -1;
            turn_ = 1;
            ai_play(value, play, turn_);
        }
        else
        {
            return game(value, val, pre);
        }
        if (winner(value))
        {
            if (turn_)
            {
                printf(" \nPlayer %d winn \n", turn_ - 1);
                break;
            }
            printf(" \nPlayer %d winn \n", turn_ + 1);
            break;
        }
        play -= 1;
    } while (play >= 0);
}

int main()
{
    srand(time(0));

    int value[9];
    for (int i = 0; i < 9; i++)
    {
        value[i] = 0;
    }
    game(value, &value, 0);
}
