from ctypes import c_int ,CDLL,POINTER
from array import array

class Game(object):

    def __init__(self):
        self.player_game =  (c_int * 9)(* array('i',[0 for x in range(9)]));
        self.player_turn = 0;
        ## Capi;
        self.Capi = CDLL("./game.so");
        #self.libc = CDLL("libc.so.6")
        #self.printf = self.libc.printf;

    def display_bord(self):
        (self.Capi.display(self.player_game));

    def user_input(self):
        user_input = int(input(f"Enter The Player value {self.player_turn}? "));
        #print(self)
        if(user_input<=9 and user_input>0):
            user_input = user_input-1;
            if(self.player_game[user_input]==0 and self.player_turn):
                self.player_game[user_input] = 1;
                self.player_turn = 0

            elif(self.player_game[user_input]==0 and not self.player_turn):
                self.player_game[user_input] = -1;
                self.player_turn = 1;

            elif(self.player_game[user_input]!=0):
                return self.user_input();
        else:
            return self.user_input();

    def Run(self):
        play = 9;
        while (play):
            if not self.Capi.win(self.player_game):
                self.display_bord();
                self.user_input();
                if play==1:
                    self.display_bord()
                play-=1;
            else:
#                print("\n");
                self.display_bord()
                print(f"player {self.player_turn} win")
                break;



print(Game().Run());


