
#include <stdio.h>

void display(int array[]){
    for (int i = 0; i < 9; i++)
    {
        if ((i + 1) % 3 == 0)
        {
            printf("%d  \n", array[i]);
        }
        else
        {
            printf("%d  ", array[i]);
        }
    }
}

int win(int w[])
{
    if ((w[0] + w[4] + w[8]) == 3 || (w[2] + w[4] + w[6]) == 3 || (w[0] + w[4] + w[8]) == -3 || (w[2] + w[4] + w[6]) == -3)
    {
        return 1;
    }
    else if ((w[0] + w[1] + w[2]) == 3 || (w[3] + w[4] + w[5]) == 3 || (w[6] + w[7] + w[8]) == 3 ||
             (w[0] + w[1] + w[2]) == -3 || (w[3] + w[4] + w[5]) == -3 || (w[6] + w[7] + w[8]) == -3)
    {
        return 1;
    }
    else if ((w[0] + w[3] + w[6] == 3) || (w[1] + w[4] + w[7] == 3) || (w[2] + w[5] + w[8] == 3) || (w[0] + w[3] + w[6] == -3) || (w[1] + w[4] + w[7] == -3) || (w[2] + w[5] + w[8] == -3))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

