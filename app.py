board = dict([(x,' ') for x in range(1,9+1)])
print(board)
def printBoard(board):
    print(board[1] + " | " + board[2] + " | " + board[3])
    print("---------")
    print(board[4] + " | " + board[5] + " | " + board[6])
    print("---------")
    print(board[7] + " | " + board[8] + " | " + board[9])
    print('\n');
    
print(printBoard(board))
def space_is_free(position):
    if board[position] == ' ':
        return True 
    return False

def insert(p,letter):
    if(space_is_free(p)):
        board[p] = letter;
        printBoard(board); 
        
        if check_draw(board):
            print("DRAWWWW :: ");
            quit();
        
        if checkWin(board):
            if letter == "X":
                print("BOT WIN");
                exit()
            else:
                print("PLAYER WINN");
                exit()
    else:
        i = int(input("ENTER NUMBER \n"))
        insert(i,letter);
            


def check_draw(n):
    for i in range(1,1+9):
        if n[i] == " ":
            return 0;
    else:
        return 1;

def checkWhichMarkWon(mark):
    if (board[1] == board[2] and board[1] == board[3] and board[1] == mark):
        return True
    elif (board[4] == board[5] and board[4] == board[6] and board[4] == mark):
        return True
    elif (board[7] == board[8] and board[7] == board[9] and board[7] == mark):
        return True
    elif (board[1] == board[4] and board[1] == board[7] and board[1] == mark):
        return True
    elif (board[2] == board[5] and board[2] == board[8] and board[2] == mark):
        return True
    elif (board[3] == board[6] and board[3] == board[9] and board[3] == mark):
        return True
    elif (board[1] == board[5] and board[1] == board[9] and board[1] == mark):
        return True
    elif (board[7] == board[5] and board[7] == board[3] and board[7] == mark):
        return True
    else:
        return False

def checkWin(board):
    if (board[1] == board[2] and board[1] == board[3] and board[1] != ' '):
        return True
    elif (board[4] == board[5] and board[4] == board[6] and board[4] != ' '):
        return True
    elif (board[7] == board[8] and board[7] == board[9] and board[7] != ' '):
        return True
    elif (board[1] == board[4] and board[1] == board[7] and board[1] != ' '):
        return True
    elif (board[2] == board[5] and board[2] == board[8] and board[2] != ' '):
        return True
    elif (board[3] == board[6] and board[3] == board[9] and board[3] != ' '):
        return True
    elif (board[1] == board[5] and board[1] == board[9] and board[1] != ' '):
        return True
    elif (board[7] == board[5] and board[7] == board[3] and board[7] != ' '):
        return True
    else:
        return False

c,p = 'X','0';
def Pgame():
    i = int(input("ENTER NUMBER 0 \n"));
    insert(i,p);
    return 

def Cgame():
    best_sc = -100;
    best_mv = 0;
    
    board_ = board.copy()
    
    for i in board_.keys():
        if board_[i] == " ":
            board_[i] = "X";
            score = min_max(board,0,5);

            board_[i] = " ";
            if (score>best_sc):
                best_sc = score;
                best_mv = i;
    print(best_mv);
    insert(best_mv,"X");

def min_max(b,is_min_max,d=0):
    
    if(checkWhichMarkWon("X")):
        return 1;
    elif(checkWhichMarkWon("0")):
        return -1;
    elif(check_draw(board)):
        return 0;
    
    if is_min_max:
        best_sc = -100;
        for i in board.keys():
            if b[i] == " ":
                b[i] = "X";
                score = min_max(board,0,d-1);
                board[i] = " ";
                if (score>best_sc):
                    best_sc = score;
        return best_sc
    else:
        best_sc = 10000;
        for i in board.keys():
            if b[i] == " ":
                b[i] = "0";
                score = min_max(board,1,d-1);
                board[i] = " ";
                if (score<best_sc):
                    best_sc = score;
        return best_sc
    
while(1):
    Cgame()
    Pgame()
    
